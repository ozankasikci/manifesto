## PAYPAD DEVELOPER MANIFEST

* Coding guide’a göre kod yazmalıyız. Bırakacağımız boşluk sayısı, parantezler arası boşluklar, tab space sayısı vesaire belli olmalı.

* Kod bizim düşmanımız. Daha çok kod daha çok hata ve daha çok bakım ihtiyacı demektir. Mümkünse daha az kod yazmalıyız, silebildiğimiz kodları silmeliyiz.
Gerekmeyen kod yazmamalıyız. (yagni) Buna gereksiz yorum satırları da dahil.

* Kod yazarken defansif olmalıyız, muhtemel hatalar kullanıcıya hiç ulaşmadan handle edilmeli. İnvalid input type, eksik input sayısı, exception’a sebep olabilecek case’ler gibi tüm ihtimalleri önceden düşünmeliyiz. Fail fast prensibini izlemeliyiz. (Kodun her tarafında basit hataları hemen check etmeliyiz, ve hata dönmeliyiz)

* Kolay test edilebilir kod, stateless ve idempotent koddur, mümkünse logic farklı fonksiyonlara ayrılmalı, stateful fonksiyonlar ve yan etkisi olmayan fonksiyonlara ayrılmalı. Bunun bize sağlayacağı fayda şudur, yan etkisi olan fonksiyonlar için bir kere test yazılır ve diğer her yerde bu tür fonksiyonlar mock’lanır.

* Yazdığımız her yeni kod için test yazmalıyız. Yeni bir test yazdığımızda, kod’da bilerek bir bug yaratmalı test’in fail ettiğinden emin olmalıyız. Red to green mantığını izlemeliyiz.

* Mümkün olduğunca constructor’da logic tutmamalıyız. Bu tarz kodu test etmek zordur. Eninde sonunda test edebilmek için dependency injection kullanmak gerekecektir.

* Mümkün olduğunda dependency injection paternini kullanmalıyız, bu bir fonksiyonun bağımlılıklarını kolayca görmemizi ve test edileblir kod yazmamızı sağlar. Bir diğer faydası fazla dependency enjekte edilen bir fonksiyonla karşılaşırsak, bu fonksiyonun çok fazla iş yaptığını kolayca anlayabilmemizdir.

* Gerektiğinde ve yapabildiğimizde düşünmeden kodu refactor etmeliyiz. Zamanında refactor edilmeyen kod, bağımlılıklar arttıkça değiştirmesi göz korkutucu daha da önemlisi rafactor etmesi çok pahalı hale gelir ve bug çıkma ihtimali artar.

* Bir fonksiyon 30 satırdan fazla ise parçalamayı düşünmeye başlamalıyız. Yeni yazılan bir modülün satır sayısı 500’ü geçmemeli gibi kodun karmaşıklığını sınırlayacak limitler getirmeliyiz.

* Analiz yapmadan, gerektiğinde chart çıkarıp planlamasını yapmadan kodlamaya başlamamalıyız.

* Başkalarının kodumuzu ve düşünme yöntemimizi sorgulamasından çekinmemeli, egodan sıyrılmalı ve daha ileriye gitmeliyiz.

* Yazdığımız kodun her daim yüksek kaliteli ve ileriye dönük olmasına dikkat etmeliyiz.

* Bir işin bittiğinden tam olarak emin olmadan, o işe bitti dememeliyiz. Ancak her yönüyle, düşünebildiğimiz bütün bug ihtimalleri ve uç case’leri de düşünerek çalıştığını gördükten sonra bitti diyebiliriz.

* Gerektiğinde pragmatik gerektiğinde mükemmeliyetçi olmalıyız.




### Sevgisiz sanat olmaz ve kod yazmak bir sanattır. Yaptığımız işin şiir yazmak, heykel yapmak, resim çizmekten farklı olmadığını unutmamalıyız.
